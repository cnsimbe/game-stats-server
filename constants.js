let constants = {
	RIOT_SERVER : 'na1.api.riotgames.com',
	RIOT_API_KEY: process.env.RIOT_API_KEY || ''
}

module.exports = Object.freeze(constants)