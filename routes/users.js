let express = require('express');
let router = express.Router();
let riotApi = require('../external/riotApi')

/* GET users listing. */
router.get('/summoner/by-name', async (req, res, next)=>{

	try {
		let userName = req.query.userName;
		let summoner = await riotApi.getSummonerByName(userName)
		res.json(summoner)
	} catch(error) {
		res.status(error.statusCode)
		res.json(JSON.parse(error.error))
	}
	
});

module.exports = router;
