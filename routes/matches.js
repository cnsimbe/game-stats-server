let express = require('express');
let router = express.Router();
let riotApi = require('../external/riotApi')

/* GET users listing. */
router.get('/summoner', async (req, res, next)=>{

	try {
		let accountId = req.query.accountId;
		let params = Object.assign({},req.query)
		let result =  await riotApi.getMatchListForAccount(accountId,params)

		let matchLists = await Promise.all(result.matches.map(match=>riotApi.getMatch(match.gameId)))

		result.matches = result.matches.map(match=>{
			match.game=matchLists.find(m=>m.gameId==match.gameId)
			return match
		})

		res.json(result)

	} catch(error) {
		res.status(error.statusCode)
		res.json(JSON.parse(error.error))
	}
	
});

module.exports = router;
