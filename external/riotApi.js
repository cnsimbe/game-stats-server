let request = require('request-promise');
let constants = require('../constants')

/* GET users listing. */

async function  getSummonerByName(userName) {
	let result = await  request.get(`https://${constants.RIOT_SERVER}/lol/summoner/v3/summoners/by-name/${userName}?api_key=${constants.RIOT_API_KEY}`)
	return JSON.parse(result)
}

async function  getMatchListForAccount(accountId, params) {
	let qString = Object.keys(params).map(key=>`${key}=${params[key]}`).join("&")
	let result = await request.get(`https://${constants.RIOT_SERVER}/lol/match/v3/matchlists/by-account/${accountId}?api_key=${constants.RIOT_API_KEY}&${qString}`)
	return JSON.parse(result)
}


async function  getMatch(matchId) {
	let result = await request.get(`https://${constants.RIOT_SERVER}/lol/match/v3/matches/${matchId}?api_key=${constants.RIOT_API_KEY}`)
	return JSON.parse(result)
}

module.exports =  { getSummonerByName, getMatchListForAccount, getMatch };
